## WordPress Standard Checklist
##### Website Name: Awesome Website "AwesomeWeb"
##### Developers: Name, Name, Name
##### Reviewers: Name, Name, Name

| ENV | URL |
|:---:|-----|
| INT | http://integration.domain.com
| UAT | http://user.domain.com
| STG | http://staging.domain.com
| PRD | http://domain.com/


### Performance
| Compliant | Non-Compliant | Notes | Configuration |
|:-------:|:-------:|:-------:|-------
|         | &check; |     | If page loads are slow, consider minifying WordPress files to improve the performance with a minifying plugin. There are many [minifying plugins](https://wordpress.org/plugins/search/minify/) available. One example is [Autoptimize](https://wordpress.org/plugins/autoptimize/), which is a plugin that can can aggregate, minify and cache scripts and styles, injects CSS in the page head by default but can also inline critical CSS and defer the aggregated full CSS, moves and defers scripts to the footer and minifies HTML.  
|         | &check; |     | If page loads are still too slow, consider implementing a caching plugin. There are a plethora of [caching plugins](https://wordpress.org/plugins/search/cache/) available. The top three contenders in most reviews are [WP Rocket](https://wp-rocket.me/), [WP Super Cache](https://wordpress.org/plugins/wp-super-cache/), and [W3 Total Cache](https://www.w3-edge.com/solutions/w3-total-cache-pro/). A recent review of caching plugins with performance metrics is provided [here](https://wpastra.com/best-wordpress-caching-plugins/). 
|         | &check; |     | WP_DEBUG is disabled: `define( 'WP_DEBUG', false );`
|         |         |     | **WordPress Themes**<br>&nbsp;&nbsp;&nbsp;*when selecting themes, choose from a reputable source, and:*
|         | &check; |     | Theme is actively being maintained&nbsp;&nbsp;:bulb:**Tip:** free is not always best
|         | &check; |     | Theme has good user ratings
|         | &check; |     | Theme has a reasonable number of active installations
|         | &check; |     | Theme has high browser compatibility
|         | &check; |     | Theme supports plugins you intend to use (*BuddyPress, BBPress, Gravity Forms, Yoast SEO, W3 Total Cache, etc.*)
|         | &check; |     | Theme is not "bloated", i.e., cluttered with features you won't use 
|         | &check; |     | Theme uses responsive design
|         | &check; |     | Theme is optimized for speed (including using web fonts sparingly)


### Configuration
| Compliant | Non-Compliant | Notes | Configuration |
|:-------:|:-------:|:-------:|-------
|         | &check; |     | Status page (Tools » Site Health) is healthy and does not show any misconfigurations
|         | &check; |     | If using an [E-mail plugin](https://wordpress.org/plugins/search/smtp/), "From" Address set to noreply@domain.com unless otherwise specified
|         | &check; |     | If social media inegration is required, there are [social media plugins](https://wordpress.org/plugins/search/social+media/) that can optimize and schedule posting/linking to pages on Facebook, Twitter, LinkedIn, etc.
|         | &check; |     | Favicon uploaded and configured
|         | &check; |     | All unused themes are removed
|         | &check; |     | All unused plugins are removed
|         | &check; |     | All DevOps configuration stored in configuration management
|         |         |     | **WordPress Plugins**<br>&nbsp;&nbsp;&nbsp;*when selecting plugins, choose from a reputable source, and:*
|         | &check; |     | Plugin is actively being maintained&nbsp;&nbsp;:bulb:**Tip:** free is not always best
|         | &check; |     | Plugin has good user ratings
|         | &check; |     | Plugin has a reasonable number of active installations


### SEO
| Compliant | Non-Compliant | Notes | Configuration |
|:-------:|:-------:|:-------:|-------
|         | &check; |     | All pages have unique page titles
|         | &check; |     | Google Analytics installed. This will require a [plugin](https://wordpress.org/plugins/search/Google+Analytics/). [Google Analytics for WordPress by MonsterInsights](https://wordpress.org/plugins/google-analytics-for-wordpress/) is recommended.
|         | &check; |     | If SEO optimization is required, there are many [SEO plugins](https://wordpress.org/plugins/search/seo/) to choose from. [Yoast SEO](https://wordpress.org/plugins/wordpress-seo/) is the top rated and the most established plugin, but other SEO plugins have received [favorable reviews](https://www.techradar.com/best/wordpress-seo-tools).
|         | &check; |     | Sitemap visible at /sitemap.xml (requires SEO plugin or manual creation)


### Monitoring & Logging
| Compliant | Non-Compliant | Notes | Configuration |
|:-------:|:-------:|:-------:|-------
|         | &check; |     | WP_DEBUG is disabled (unless actively troubleshooting an issue)
|         | &check; |     | Status page (Tools » Site Health) reviewed for errors and recommendations
|         | &check; |     | Consider using a [security plugin](https://wordpress.org/plugins/search/security/) for file integrity monitoring, failed login attempts, malware scanning, 2FA, etc. Many security plugins, like [Securi](https://sucuri.net/wordpress-security-plugin) and [Wordfence](https://wordpress.org/plugins/wordfence/), provide audit logging, integrity checking, email alerts, and other features via registration with their remote API service. These services typically require a license and charge a fee.
|         | &check; |     | External monitoring configured


### Security
| Compliant | Non-Compliant | Notes | Configuration |
|:-------:|:-------:|:-------:|-------
|         | &check; |     | Account registration limited to Administrators only, unless otherwise required
|         | &check; |     | WordPress core is updated to latest supported version: *Check at least weekly*
|         | &check; |     | All plugins and themes updated to latest supported versions: *Check at least weekly*
|         | &check; |     | All elevated user accounts correlate to a single person
|         | &check; |     | Commenting is disabled unless otherwise specified. If commenting is enabled then use an [antispam plugin](https://wordpress.org/plugins/search/antispam/). [Akismet](https://wordpress.org/plugins/akismet/) is recommended.
|         | &check; |     | UID 1 username is changed to something other than ‘admin’
|         | &check; |     | Password Policy: Natively WordPress generates random passwords for new user accounts and makes use of a ZXCVBN password strength estimator to evaulate password strength. This operation is consistent with modern password policy advice to eliminate character-composition requirements for password generation. Administratiors should make full use of these tools to generate strong passwords for all user accounts. Additionally, there are [password policy plugins](https://wordpress.org/plugins/search/password+policy/) available that can add finer grain control over password generation and usage if required.
|         |         |     | **WordPress Hardening**<br>&nbsp;&nbsp;&nbsp;*the following can be implemented either through most [security plugins](https://wordpress.org/plugins/search/security/) or manually*
|         | &check; |     | Disable file editing: `define( 'DISALLOW_FILE_EDIT', true );`
|         | &check; |     | Disable PHP file execution in */wp-content/uploads/* either via security plugin or manually. To do it manually, create or add to the .htaccess file in the directory: <br>&nbsp;&nbsp;&nbsp;&nbsp;```<Files *.php>deny from all</Files>```
|         | &check; |     | Use a [plugin](https://wordpress.org/plugins/search/Limit+Login+Attempts/) to limit login attempts. [Limit Login Attempts Reloaded](https://wordpress.org/plugins/limit-login-attempts-reloaded/) is recommended. Recommended Lockout configuration is:<br><ul><li>5 allowed retries</li><li>30 minutes lockout</li><li>6 lockouts increase lockout time to 24 hours</li><li>1 hour until retries are reset</li></ul>
|         | &check; |     | 2FA applied to content administrator role and above, if required. Implementing 2FA requires a [plugin](https://wordpress.org/plugins/search/2fa/).
|         | &check; |     | Rename the login URL using a [plugin](https://wordpress.org/plugins/search/hide+login/). [WPS Hide Login](https://wordpress.org/plugins/wps-hide-login/) is recommended. This plugin is also useful if you need to present the user with a login warning banner prior to navigating to the login page.
|         | &check; |     | If a [security plugin](https://wordpress.org/plugins/search/security/) is used, specify the E-mail addresses to notify when updates are available: <you@domain.com>
|         | &check; |     | If a [security plugin](https://wordpress.org/plugins/search/security/) is used, specify the E-mail notification threshold: *Only security updates*
|         | &check; |     | Disable directory indexing and browsing by adding `Options -Indexes` to the end of the .htaccess file in the website’s root directory 
|         | &check; |     | Disable XML-RPC either by using a plugin or manually. There are numerous guides on the Internet that describe [how to do it](https://www.wpbeginner.com/plugins/how-to-disable-xml-rpc-in-wordpress/). 
|         | &check; |     | If not already done in the distro, move the *wp-config.php* file one folder above the WordPress root directory
|         | &check; |     | Add HTTP Headers to the site. The following list is provided as a starting point, but it may need to be tailored based on the WordPress site's functional requirements:<br><ul><li>strict-transport-security: max-age=31536000; includeSubDomains; preload;</li><li>x-robots-tag: nofollow</li><li>access-control-allow-origin: *</li><li>access-control-allow-credentials: true</li><li>access-control-allow-methods: POST,GET,OPTIONS</li><li>access-control-allow-headers: origin</li><li>referrer-policy: no-referrer-when-downgrade</li><li>x-frame-options: deny</li><li>x-xss-protection: 1; mode=block</li><li>x-content-type-options: nosniff</li><li>content-security-policy: default-src 'none'; script-src 'self'; connect-src 'self'; img-src 'self'; style-src 'self';base-uri 'self';form-action 'self'</li></ul>


### POST LAUNCH
| Compliant | Non-Compliant | Notes | Configuration |
|:-------:|:-------:|:-------:|-------
|         |         |     | HTTP redirects to HTTPS
|         |         |     | Sitemap shows production domain as base url
|         |         |     | Sitemap submitted to Google Webmaster Tools
|         |         |     | External monitoring confirmed receiving data
|         |         |     | Google Analytics verified from GA dashboard
|         |         |     | Google Analytics only shows data from the prod hostname
|         |         |     | Website is able to send E-mails, if require
|         |         |     | Broken link report generated.

1. Not applicable.
2. Reason why not compliant #1.
3. Reason why not compliant #2.
4. ...


##### Checklist v202205xx